# Shipments

This project was created using MERN stack: Mongo + Express + React + Node. The React part was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
Open the server in http://localhost:5000

The page will reload if you make edits.
You will also see any lint errors in the console.

Inside client folder:

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm coverage`

Shows the actual coverage of the application.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

## Deployment

This site is deployed on Heroku. You can see a live demo [here](https://shipments123.herokuapp.com/)
