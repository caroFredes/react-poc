const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const Shipment = require('../../models/Shipment');

// This is an example API to manage the get and post needed for the React app.

// @route   GET api/shipments
// @desc    Get all shipments info
// @access  Public
router.get('/', async (req, res) => {
  try {
    // Returns all shipments found on database
    const shipments = await Shipment.find();
    res.json(shipments);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route    GET api/shipments/:shipment_id
// @desc     Get shipment by ID
// @access   Public
router.get('/:shipment_id', async (req, res) => {
  try {
    const shipment = await Shipment.findOne({
      _id: req.params.shipment_id
    });

    if (!shipment) return res.status(400).json({ msg: 'Shipment not found' });

    res.json(shipment);
  } catch (err) {
    console.error(err.message);
    if (err.kind == 'ObjectId') {
      return res.status(400).json({ msg: 'Shipment not found' });
    }
    res.status(500).send('Server Error');
  }
});

// @route   POST api/shipments
// @desc    Post user
// @access  Public
router.post(
  '/',
  [
    check('name', 'Name is required')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    // If there are errors
    if (!errors.isEmpty()) {
      // Return Bad request
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      id,
      name,
      cargo,
      mode,
      type,
      destination,
      origin,
      userId
    } = req.body;

    // Build shipment object
    const shipmentFields = {};
    if (name) shipmentFields.name = name;
    if (cargo) shipmentFields.cargo = cargo;
    if (mode) shipmentFields.mode = mode;
    if (type) shipmentFields.type = type;
    if (destination) shipmentFields.destination = destination;
    if (origin) shipmentFields.origin = origin;
    if (userId) shipmentFields.userId = userId;

    try {
      let shipment = await Shipment.findOneAndUpdate(
        { _id: id },
        { $set: shipmentFields },
        { new: true, upsert: true }
      );

      return res.json(shipment);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  }
);

module.exports = router;
