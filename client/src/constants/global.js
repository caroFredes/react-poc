// Routes
export const ROOT = '/';
export const DETAILS = '/shipment';

// API url
export const url = '/api/shipments';
