//*--------------------LIBRARIES--------------------*//
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
//*--------------INTERNAL DEPENDENCIES--------------*//
import ShipmentDetails from '../../components/ShipmentDetails';
import {
  updateShipmentName,
  getShipmentById
} from '../../redux/shipments/action';

class ShipmentDetailsContainer extends Component {
  componentDidMount() {
    const { selectedShipment } = this.props;
    if (!selectedShipment) {
      const { match, onGetShipmentById } = this.props;
      const { shipmentId } = match.params;

      onGetShipmentById({ id: shipmentId });
    }
  }

  render() {
    const { onUpdateShipmentName, selectedShipment } = this.props;

    return (
      <ShipmentDetails
        shipment={selectedShipment}
        onUpdateShipmentName={onUpdateShipmentName}
      />
    );
  }
}

const mapStateToProps = state => {
  const selectedShipment = state.shipmentsReducer.selectedShipment;

  return {
    selectedShipment
  };
};

const mapDispatchToProps = dispatch => ({
  onUpdateShipmentName: payload => dispatch(updateShipmentName(payload)),
  onGetShipmentById: payload => dispatch(getShipmentById(payload))
});

ShipmentDetailsContainer.defaultProps = {
  selectedShipment: null
};

ShipmentDetailsContainer.propTypes = {
  selectedShipment: PropTypes.shape({
    cargo: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string,
        description: PropTypes.string,
        volume: PropTypes.string
      })
    ),
    destination: PropTypes.string,
    id: PropTypes.string,
    key: PropTypes.string,
    mode: PropTypes.string,
    name: PropTypes.string,
    origin: PropTypes.string,
    services: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string
      })
    ),
    status: PropTypes.string,
    total: PropTypes.string,
    type: PropTypes.string,
    userId: PropTypes.string
  }),
  // Required Methods:
  onUpdateShipmentName: PropTypes.func.isRequired,
  onGetShipmentById: PropTypes.func.isRequired
};

const ShipmentDetailsRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ShipmentDetailsContainer);

export { ShipmentDetailsContainer };
export default ShipmentDetailsRedux;
