//*--------------------LIBRARIES--------------------*//
import React, { Component } from 'react';
import { connect } from 'react-redux';
//*--------------INTERNAL DEPENDENCIES--------------*//
import ShipmentsList from '../../components/ShipmentsList';
import {
  loadShipments,
  updateSelectedShipment
} from '../../redux/shipments/action';

class ShipmentsContainer extends Component {
  componentDidMount() {
    const { onGetAllShipments } = this.props;
    onGetAllShipments();
  }

  render() {
    const { shipments, onGetDetails } = this.props;
    return <ShipmentsList shipments={shipments} onGetDetails={onGetDetails} />;
  }
}

const mapStateToProps = state => {
  const shipments = state.shipmentsReducer.shipments;

  return {
    shipments
  };
};

const mapDispatchToProps = dispatch => ({
  onGetAllShipments: () => dispatch(loadShipments()),
  onGetDetails: payload => dispatch(updateSelectedShipment(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShipmentsContainer);
