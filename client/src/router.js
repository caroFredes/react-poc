//*--------------------LIBRARIES--------------------*//
import React from 'react';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
//*--------------INTERNAL DEPENDENCIES--------------*//
import ShipmentsContainer from './containers/Shipments';
import ShipmentDetailsContainer from './containers/ShipmentDetails';
import AppHeader from './components/Header';
// Constants
import { ROOT, DETAILS } from './constants/global';

const MainRouter = () => (
  <Router>
    <AppHeader />
    <Switch>
      <Route exact path={ROOT} component={ShipmentsContainer} />
      <Route
        path={`${DETAILS}/:shipmentId`}
        component={ShipmentDetailsContainer}
      />
    </Switch>
  </Router>
);

export default MainRouter;
