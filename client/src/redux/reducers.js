//*--------------------LIBRARIES--------------------*//
import { combineReducers } from 'redux';
//*--------------INTERNAL DEPENDENCIES--------------*//
import shipmentsReducer from './shipments/reducer';

const rootReducer = combineReducers({
  shipmentsReducer
});

export default rootReducer;
