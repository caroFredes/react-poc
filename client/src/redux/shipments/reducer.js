//*--------------INTERNAL DEPENDENCIES--------------*//
import actionTypes from './actionTypes';

const updateNameInShipment = (state, action) => {
  const { shipments } = state;
  const { payload } = action;
  const { id, name } = payload;
  return shipments.map(item => {
    // Find the item with the matching id
    if (item.id === id) {
      // Return a new object
      return {
        ...item, // copy the existing item
        name // replace name
      };
    }

    // Leave every other item unchanged
    return item;
  });
};

const addKeyToShipments = action => {
  const { payload } = action;
  return payload.map(item => {
    return {
      ...item, // copy the existing item
      key: item.id // add key
    };
  });
};

const defaultState = {
  shipments: [],
  selectedShipment: null
};

const shipmentsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_SHIPMENTS_SUCCESS: {
      return { ...state, shipments: addKeyToShipments(action) };
    }
    case actionTypes.GET_SHIPMENT_BY_ID_SUCCESS: {
      return { ...state, selectedShipment: action.payload };
    }
    case actionTypes.UPDATE_SELECTED_SHIPMENT: {
      return { ...state, selectedShipment: action.payload };
    }
    case actionTypes.UPDATE_SHIPMENT_NAME_SUCCESS: {
      return { ...state, shipments: updateNameInShipment(state, action) };
    }

    default:
      return state;
  }
};

export default shipmentsReducer;
