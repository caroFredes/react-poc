//*--------------------LIBRARIES--------------------*//
import axios from 'axios';
//*--------------INTERNAL DEPENDENCIES--------------*//
import actionTypes from './actionTypes';
import { url } from '../../constants/global';

export const getAllShipmentsSuccess = payload => ({
  type: actionTypes.GET_ALL_SHIPMENTS_SUCCESS,
  payload
});

export const getShipmentByIdSuccess = shipment => ({
  type: actionTypes.GET_SHIPMENT_BY_ID_SUCCESS,
  payload: shipment
});

export const updateShipmentNameSuccess = payload => ({
  type: actionTypes.UPDATE_SHIPMENT_NAME_SUCCESS,
  payload
});

export const updateSelectedShipment = payload => ({
  type: actionTypes.UPDATE_SELECTED_SHIPMENT,
  payload
});

export const getShipmentById = payload => {
  const { id } = payload;
  return async dispatch => {
    try {
      const res = await axios.get(`${url}/${id}`);

      dispatch(getShipmentByIdSuccess(res.data));
    } catch (error) {
      console.log(error);
    }
  };
};

export const updateShipmentName = payload => {
  const { id, name } = payload;
  return async dispatch => {
    try {
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

      await axios.post(`${url}`, { id, name }, config);

      dispatch(updateShipmentNameSuccess(payload));
    } catch (error) {
      console.log(error);
    }
  };
};

export const loadShipments = () => {
  return async dispatch => {
    try {
      const res = await axios.get(url);
      dispatch(getAllShipmentsSuccess(res.data));
    } catch (error) {
      console.log(error);
    }
  };
};
