const shipmentExample = {
  cargo: [
    { type: "Fabric", description: "1000 Blue T-shirts", volume: "2" },
    { type: "Fabric", description: "2000 Green T-shirts", volume: "3" }
  ],
  destination: "Saarbrücker Str. 38, 10405 Berlin",
  id: "S1000",
  key: "S1000",
  mode: "sea",
  name: "T-shirts from Shanghai to Ham",
  origin: "Shanghai Port",
  services: [{ type: "customs" }],
  status: "ACTIVE",
  total: "1000",
  type: "FCL",
  userId: "U1000"
};

const shipmentListExample = [
  {
    cargo: [
      { 
        type: "Fabric",
        description: "1000 Blue T-shirts",
        volume: "2"
      },
      {
        type: "Fabric",
        description: "2000 Green T-shirts",
        volume: "3"
      }
    ],
    destination: "Saarbrücker",
    id: "S1000",
    key: "S1000",
    mode: "sea",
    name: "T-shirts from Shanghai to Ham",
    origin: "Shanghai Port",
    services: [{ type: "customs" }],
    status: "ACTIVE",
    total: "1000",
    type: "FCL",
    userId: "U1000"
  },
  {
    id: "S1002",
    key: "S1002",
    name: "PO89634",
    cargo: [
      {
        type: "Bikes model 27X",
        description: "100 Bikes model 27X",
        volume: "100"
      }
    ],
    mode: "air",
    type: "LCL",
    destination: "Berlin",
    origin: "Berlin Port",
    services: [
      {
        type: "customs"
      }
    ],
    total: "10000",
    status: "COMPLETED",
    userId: "U1001"
  },
  {
    id: "S1003",
    key: "S1003",
    name: "PO89634, PO27X",
    cargo: [
      {
        type: "Bikes model 27X",
        description: "100 Bikes model 27X",
        volume: "100"
      }
    ],
    mode: "air",
    type: "LCL",
    destination: "Berlin",
    origin: "Berlin Port",
    services: [
      {
        type: "customs"
      }
    ],
    total: "10000",
    status: "COMPLETED",
    userId: "U1001"
  },
  {
    id: "S1004",
    key: "S1004",
    name: "PO89634, PO27X",
    cargo: [
      {
        type: "Bikes model 27X",
        description: "100 Bikes model 27X",
        volume: "100"
      }
    ],
    mode: "air",
    type: "LCL",
    destination: "Zagazig",
    origin: "Zabol",
    services: [
      {
        type: "customs"
      }
    ],
    total: "10000",
    status: "COMPLETED",
    userId: "U1001"
  }
];

export { shipmentExample, shipmentListExample };
