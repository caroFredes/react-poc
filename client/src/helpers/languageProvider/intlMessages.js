//*--------------------LIBRARIES--------------------*//
import React from 'react';
//*--------------INTERNAL DEPENDENCIES--------------*//
import { injectIntl, FormattedMessage } from 'react-intl';

/**
 * To know how to use it:
 * @see https://github.com/yahoo/react-intl
 */
const InjectMessage = props => <FormattedMessage {...props} />;

export default injectIntl(InjectMessage, {
  withRef: false
});
