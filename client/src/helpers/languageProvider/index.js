//*--------------------LIBRARIES--------------------*//
import { addLocaleData } from 'react-intl';
import locale_en from 'react-intl/locale-data/en';
import locale_es from 'react-intl/locale-data/es';
//*--------------INTERNAL DEPENDENCIES--------------*//
import messages_es from './locales/es.json';
import messages_en from './locales/en.json';

addLocaleData([...locale_en, ...locale_es]);

const messages = {
  es: messages_es,
  en: messages_en
};

const language =
  (navigator.languages && navigator.languages[0]) ||
  navigator.language ||
  navigator.userLanguage ||
  'en';

export { messages, language };
