//*--------------------LIBRARIES--------------------*//
import React from 'react';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
//*--------------INTERNAL DEPENDENCIES--------------*//
import MainRouter from './router';
import configureStore from './redux/store';
import { language, messages } from './helpers/languageProvider';

const store = configureStore();

const App = () => (
  <IntlProvider locale={language} messages={messages[language]}>
    <Provider store={store}>
      <MainRouter />
    </Provider>
  </IntlProvider>
);

export default App;
