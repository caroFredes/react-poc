//*--------------------LIBRARIES--------------------*//
import styled from 'styled-components';
//*--------------INTERNAL DEPENDENCIES--------------*//
// Constant
import { PALETTE } from '../../constants/palette';

const ResultWrapper = styled.div`
  button {
    margin-left: 20px;
  }
`;
ResultWrapper.displayName = 'ResultWrapper';

const EditableWrapper = styled.div`
  display: flex;
  button {
    margin-left: 15px;

    &.js-save-btn {
      background: ${PALETTE.green};
      border-color: ${PALETTE.green};
    }
  }
`;
EditableWrapper.displayName = 'EditableWrapper';

export { EditableWrapper, ResultWrapper };
