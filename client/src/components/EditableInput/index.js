//*--------------------LIBRARIES--------------------*//
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'antd';
//*--------------INTERNAL DEPENDENCIES--------------*//
import { EditableWrapper, ResultWrapper } from './index.style';

/**
 * EditableInput Component.
 *
 * This component render a span with the actual value passed by props and a button
 * that allows edition.
 * On edition mode, the user can update the value, save or cancel, and the component
 * will trigger the callback passed by props.
 *
 */

class EditableInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      editable: false,
      originalValue: ''
    };
  }

  componentDidMount() {
    const { value } = this.props;

    this.setState({
      value,
      originalValue: value
    });
  }

  handleChangeInput = event => {
    const { target } = event;
    const { value } = target;
    this.setState({ value });
  };

  handleCloseEdit = () => {
    const { originalValue } = this.state;

    this.setState({
      value: originalValue,
      editable: false
    });
  };

  handleEdit = () => {
    this.setState({
      editable: true
    });
  };

  handleSaveEdit = () => {
    const { onChange } = this.props;
    const { originalValue, value } = this.state;
    if (value.length > 0 && originalValue !== value) {
      onChange(value);
    }
    this.setState({
      editable: false
    });
  };

  render() {
    const { value, editable } = this.state;
    return (
      <>
        {editable ? (
          <EditableWrapper>
            <Input
              defaultValue={value}
              autoFocus
              onChange={this.handleChangeInput}
              onPressEnter={this.handleSaveEdit}
            />
            <Button
              type='primary'
              shape='circle'
              icon='check'
              className='js-save-btn'
              onClick={this.handleSaveEdit}
            />
            <Button
              type='danger'
              shape='circle'
              icon='close'
              className='js-close-btn'
              onClick={this.handleCloseEdit}
            />
          </EditableWrapper>
        ) : (
          <ResultWrapper>
            <span>{value}</span>
            <Button
              type='primary'
              shape='circle'
              icon='edit'
              onClick={this.handleEdit}
            />
          </ResultWrapper>
        )}
      </>
    );
  }
}

EditableInput.propTypes = {
  // Required:
  value: PropTypes.string.isRequired,
  // Required Methods:
  onChange: PropTypes.func.isRequired
};

export default EditableInput;
