//*--------------------LIBRARIES--------------------*//
import React from 'react';
import { mount } from 'enzyme';
//*--------------INTERNAL DEPENDENCIES--------------*//
import EditableInput from './index';
import { Button } from 'antd';

describe('<EditableInput />', () => {
  let wrapper;

  const requiredProps = {
    value: 'title',
    onChange: jest.fn()
  };

  beforeAll(() => {
    wrapper = mount(<EditableInput {...requiredProps} />);
  });

  it('should render a Editable component when clicked on edit button', () => {
    wrapper
      .find(Button)
      .first()
      .simulate('click');
    expect(wrapper.find(EditableInput)).toHaveLength(1);
  });

  it('Editable component renders correctly', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });

  it('should update value of input to foo when clicked on save button', () => {
    const saveBtn = wrapper.find('button.ant-btn.js-save-btn');
    const editableInput = wrapper.find('input');

    editableInput.instance().value = 'foo';
    editableInput.simulate('change');
    saveBtn.simulate('click');
    wrapper.update();
    expect(wrapper.find('span')).toHaveLength(1);
    expect(wrapper.find('span').text()).toBe('foo');
  });

  it('should remain same value of input when clicked on close button', () => {
    wrapper
      .find(Button)
      .first()
      .simulate('click');
    wrapper.update();
    const closeBtn = wrapper.find('button.ant-btn.js-close-btn');
    const editableInput = wrapper.find('input');

    editableInput.instance().value = 'foo';
    editableInput.simulate('change');
    closeBtn.simulate('click');
    wrapper.update();
    expect(wrapper.find('span')).toHaveLength(1);
    expect(wrapper.find('span').text()).toBe(requiredProps.value);
  });

  it('should remain same value of input when trying to save same value', () => {
    wrapper
      .find(Button)
      .first()
      .simulate('click');
    wrapper.update();
    const saveBtn = wrapper.find('button.ant-btn.js-save-btn');
    const editableInput = wrapper.find('input');

    editableInput.instance().value = requiredProps.value;
    editableInput.simulate('change');
    saveBtn.simulate('click');
    wrapper.update();
    expect(wrapper.find('span')).toHaveLength(1);
    expect(wrapper.find('span').text()).toBe(requiredProps.value);
  });

  it('should remain same value of input when trying to save empty value', () => {
    wrapper
      .find(Button)
      .first()
      .simulate('click');
    wrapper.update();
    const saveBtn = wrapper.find('button.ant-btn.js-save-btn');
    const editableInput = wrapper.find('input');

    editableInput.instance().value = '';
    editableInput.simulate('change');
    saveBtn.simulate('click');
    wrapper.update();
    expect(wrapper.find('span')).toHaveLength(1);
    expect(wrapper.find('span').text()).toBe('');
  });
});
