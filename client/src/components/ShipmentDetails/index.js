//*--------------------LIBRARIES--------------------*//
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Descriptions, List, Button } from 'antd';
import { Link } from 'react-router-dom';
//*--------------INTERNAL DEPENDENCIES--------------*//
import EditableInput from '../EditableInput';
import DetailsWrapper from './index.style';
import IntlMessages from '../../helpers/languageProvider/intlMessages';
// Constants
import { ROOT } from '../../constants/global';

/**
 * ShipmentDetails Component.
 *
 * This component render the shipment details that the API provides.
 * Also contains a button to go back to the home page.
 *
 */
class ShipmentDetails extends Component {
  handleNameUpdate = newName => {
    const { onUpdateShipmentName, shipment } = this.props;
    onUpdateShipmentName({ id: shipment._id, name: newName });
  };

  render() {
    const { shipment } = this.props;
    return (
      <DetailsWrapper>
        <Link to={ROOT} className='back-button'>
          <Button type='primary'>
            <IntlMessages id='general.goBack' />
          </Button>
        </Link>
        {shipment && (
          <Descriptions
            title={<IntlMessages id='shipments.subtitle' />}
            bordered
          >
            <Descriptions.Item
              label={<IntlMessages id='shipments.name' />}
              span={3}
            >
              <EditableInput
                value={shipment.name}
                onChange={this.handleNameUpdate}
              />
            </Descriptions.Item>
            <Descriptions.Item
              label={<IntlMessages id='shipments.cargo' />}
              span={3}
            >
              <List
                dataSource={shipment.cargo}
                renderItem={item => (
                  <List.Item>
                    {item.volume} {item.description}, type: {item.type}
                  </List.Item>
                )}
              />
            </Descriptions.Item>
            <Descriptions.Item label={<IntlMessages id='shipments.mode' />}>
              {shipment.mode}
            </Descriptions.Item>
            <Descriptions.Item
              label={<IntlMessages id='shipments.type' />}
              span={2}
            >
              {shipment.type}
            </Descriptions.Item>
            <Descriptions.Item
              label={<IntlMessages id='shipments.destination' />}
            >
              {shipment.destination}
            </Descriptions.Item>
            <Descriptions.Item
              label={<IntlMessages id='shipments.origin' />}
              span={2}
            >
              {shipment.origin}
            </Descriptions.Item>
          </Descriptions>
        )}
      </DetailsWrapper>
    );
  }
}

ShipmentDetails.defaultProps = {
  shipment: null
};

ShipmentDetails.propTypes = {
  shipment: PropTypes.shape({
    cargo: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string,
        description: PropTypes.string,
        volume: PropTypes.string
      })
    ),
    destination: PropTypes.string,
    id: PropTypes.string,
    key: PropTypes.string,
    mode: PropTypes.string,
    name: PropTypes.string,
    origin: PropTypes.string,
    services: PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.string
      })
    ),
    status: PropTypes.string,
    total: PropTypes.string,
    type: PropTypes.string,
    userId: PropTypes.string
  }),
  // Required Methods:
  onUpdateShipmentName: PropTypes.func.isRequired
};

export default ShipmentDetails;
