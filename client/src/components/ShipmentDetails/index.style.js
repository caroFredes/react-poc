//*--------------------LIBRARIES--------------------*//
import styled from 'styled-components';

const DetailsWrapper = styled.div`
  padding: 20px;

  .back-button {
    position: absolute;
    right: 20px;
  }
`;
DetailsWrapper.displayName = 'DetailsWrapper';

export default DetailsWrapper;
