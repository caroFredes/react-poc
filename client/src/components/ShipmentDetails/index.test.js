//*--------------------LIBRARIES--------------------*//
import React from 'react';
import { mount } from 'enzyme';
import { BrowserRouter } from 'react-router-dom';
import { Button } from 'antd';
//*--------------INTERNAL DEPENDENCIES--------------*//
import ShipmentDetails from './index';
import { shipmentExample } from '../../models/shipments';
import EditableInput from '../EditableInput';

describe('<ShipmentDetails />', () => {
  describe('Empty shipment', () => {
    let wrapper;
    const requiredProps = {
      onUpdateShipmentName: jest.fn()
    };

    beforeAll(() => {
      wrapper = mount(
        <BrowserRouter>
          <ShipmentDetails {...requiredProps} />
        </BrowserRouter>
      );
    });

    it('should render ShipmentDetails component', () => {
      expect(wrapper.find(ShipmentDetails)).toHaveLength(1);
    });

    it('ShipmentDetails component renders correctly with empty shipment', () => {
      expect(wrapper.debug()).toMatchSnapshot();
    });
  });

  describe('With shipment info', () => {
    let wrapper;
    const requiredProps = {
      onUpdateShipmentName: jest.fn(),
      shipment: shipmentExample
    };

    beforeAll(() => {
      wrapper = mount(
        <BrowserRouter>
          <ShipmentDetails {...requiredProps} />
        </BrowserRouter>
      );
    });
    it('ShipmentDetails component renders correctly with shipment info', () => {
      expect(wrapper.debug()).toMatchSnapshot();
    });

    it('should render a Editable component', () => {
      expect(wrapper.find(EditableInput)).toHaveLength(1);
      wrapper
        .find(Button)
        .first()
        .simulate('click');
      wrapper.update();
    });
  });
});
