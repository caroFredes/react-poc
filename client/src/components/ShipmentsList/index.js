//*--------------------LIBRARIES--------------------*//
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Input, Button, Icon } from 'antd';
import { injectIntl } from 'react-intl';
//*--------------INTERNAL DEPENDENCIES--------------*//
import IntlMessages from '../../helpers/languageProvider/intlMessages';
// Constants
import { DETAILS } from '../../constants/global';

/**
 * ShipmentsList Component.
 *
 * When there's data, it will render a table using Ant design table.
 * It has 4 colums: Name, Origin, Destination, Id
 * The user can sort by Origin and Destination and can search by Id.
 *
 */
class ShipmentsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
  }

  getColumnSearchProps = (dataIndex, placeholder) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
      <div>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={placeholder}
          value={selectedKeys[0]}
          onChange={e =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
        />
        <Button
          type='primary'
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon='search'
          size='small'
        >
          <IntlMessages id='general.search' />
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size='small'>
          <IntlMessages id='general.reset' />
        </Button>
      </div>
    ),
    filterIcon: () => <Icon type='search' />,
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    render: text => <span>{text}</span>
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  LinkCell = item => {
    const { onGetDetails } = this.props;

    return (
      <Link
        to={{
          pathname: `${DETAILS}/${item._id}`
        }}
        onClick={() => onGetDetails(item)}
      >
        <span>{item.name}</span>
      </Link>
    );
  };

  sorterStrings = (a, b) => {
    const nameA = a.toUpperCase(); // ignore upper and lowercase
    const nameB = b.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // Equal
    return 0;
  };

  render() {
    const { shipments, intl } = this.props;
    // This is a workaround to add a formated message as a placeholder. Another way to do this
    // will be creating a component for cases like this
    const placeholder = intl.formatMessage({
      id: 'general.search.placeholder'
    });

    // Data to be used in the Ant table
    const columns = [
      {
        // column title
        title: <IntlMessages id='shipments.name' />,
        // colunm key
        key: 'name',
        // column width
        width: '35%',
        // generating value for cell
        render: shipment => this.LinkCell(shipment)
      },
      {
        title: <IntlMessages id='shipments.origin' />,
        dataIndex: 'origin',
        width: '25%',
        // Adding Antd sorting functionality
        sorter: (a, b) => this.sorterStrings(a.origin, b.origin),
        sortDirections: ['descend', 'ascend']
      },
      {
        title: <IntlMessages id='shipments.destination' />,
        dataIndex: 'destination',
        width: '25%',
        sorter: (a, b) => this.sorterStrings(a.destination, b.destination),
        sortDirections: ['descend', 'ascend']
      },
      {
        title: <IntlMessages id='shipments.id' />,
        dataIndex: '_id',
        width: '15%',
        // Adding Antd searching functionality
        ...this.getColumnSearchProps('id', placeholder)
      }
    ];

    return (
      <>
        {shipments && (
          <Table
            columns={columns}
            rowKey='_id'
            dataSource={shipments}
            pagination={{
              defaultPageSize: 20
            }}
          />
        )}
      </>
    );
  }
}

ShipmentsList.defaultProps = {
  shipments: []
};

ShipmentsList.propTypes = {
  shipments: PropTypes.arrayOf(
    PropTypes.shape({
      cargo: PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.string,
          description: PropTypes.string,
          volume: PropTypes.string
        })
      ),
      destination: PropTypes.string,
      id: PropTypes.string,
      key: PropTypes.string,
      mode: PropTypes.string,
      name: PropTypes.string,
      origin: PropTypes.string,
      services: PropTypes.arrayOf(
        PropTypes.shape({
          type: PropTypes.string
        })
      ),
      status: PropTypes.string,
      total: PropTypes.string,
      type: PropTypes.string,
      userId: PropTypes.string
    })
  ),
  // Required Methods:
  onGetDetails: PropTypes.func.isRequired
};

export default injectIntl(ShipmentsList);

//export default ShipmentsList;
