//*--------------------LIBRARIES--------------------*//
import React from 'react';
import { mount, shallow } from 'enzyme';
import { BrowserRouter } from 'react-router-dom';
import { Table } from 'antd';
//*--------------INTERNAL DEPENDENCIES--------------*//
import ShipmentsList from './index';
import { shipmentListExample } from '../../models/shipments';

describe('<ShipmentsList />', () => {
  describe('Test cases with shallow render for snapshot testing', () => {
    let wrapper;
    const requiredProps = {
      onGetDetails: jest.fn()
    };

    beforeAll(() => {
      wrapper = shallow(<ShipmentsList {...requiredProps} />);
    });

    it('should rendƒer ShipmentsList component', () => {
      expect(wrapper.find(Table)).toHaveLength(1);
    });

    it('ShipmentsList component renders correctly with empty shipmentS', () => {
      expect(wrapper.debug()).toMatchSnapshot();
    });

    it('ShipmentsList component renders correctly with shipments info', () => {
      wrapper.setProps({ shipments: shipmentListExample });
      expect(wrapper.debug()).toMatchSnapshot();
    });
  });

  describe('Test cases with mount render for behaviour testing', () => {
    let wrapper;
    const props = {
      onGetDetails: jest.fn(),
      shipments: shipmentListExample
    };
    const sorterStrings = jest.fn();
    const handleReset = jest.fn();
    beforeAll(() => {
      wrapper = mount(
        <BrowserRouter>
          <ShipmentsList
            {...props}
            sorterStrings={sorterStrings}
            handleReset={handleReset}
          />
        </BrowserRouter>
      );
    });

    it('should filter by id', () => {
      // Get reference to dropdown for search
      const filterByIdInput = wrapper.find(
        '.ant-table-filter-dropdown .ant-input'
      );
      // Get reference to search button
      const saveButton = wrapper
        .find('.ant-table-filter-dropdown Button')
        .first();
      // update value for search
      filterByIdInput.instance().value = 'S1004';
      filterByIdInput.simulate('change');
      // Simulate search click
      saveButton.simulate('click');
      wrapper.update();
      // Now, the table should have only one row
      expect(wrapper.find('tbody tr')).toHaveLength(1);
      // and the data-row-key that is the same as the id of the element, should be S1004
      expect(wrapper.find(`tr[data-row-key='S1004']`)).toHaveLength(1);
    });

    it('should reset the filter id', () => {
      // The table should have only one row because was filtered on the previous one
      expect(wrapper.find('tbody tr')).toHaveLength(1);

      // Get reference to reset search button
      const resetButton = wrapper
        .find('.ant-table-filter-dropdown Button')
        .last();
      // Simulate search click
      resetButton.simulate('click');
      wrapper.update();

      // Now, the table should have the original four rows
      expect(wrapper.find('tbody tr')).toHaveLength(4);
    });

    it('should call the onGetDetails function when click on link to details', () => {
      // Get reference to one of the element rows
      const tableRow = wrapper.find(`tr[data-row-key='S1004']`);
      // For now, there is only one a that is the one to link to details page
      const linkCell = tableRow.find('a');

      linkCell.simulate('click');
      // the prop onGetDetails should be called
      expect(props.onGetDetails.mock.calls.length).toBe(1);
    });
  });
});
