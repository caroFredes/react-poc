//*--------------------LIBRARIES--------------------*//
import styled from 'styled-components';
//*--------------INTERNAL DEPENDENCIES--------------*//
import { Table } from 'antd';

const TableWrapper = styled(Table)`
  max-width: 700px;
  margin: 50px auto;

  .userAvatar {
    .ant-avatar {
      margin-right: 10px;
    }
  }
`;

TableWrapper.displayName = 'TableWrapper';

export default TableWrapper;
