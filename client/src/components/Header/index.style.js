//*--------------------LIBRARIES--------------------*//
import styled from 'styled-components';
//*--------------INTERNAL DEPENDENCIES--------------*//
import { Layout } from 'antd';
// Constant
import { PALETTE } from '../../constants/palette';

const { Header } = Layout;

const HeaderWrapper = styled(Header)`
  &.ant-layout-header  {
    color: ${PALETTE.white};
    font-size: 20px;
    padding: 0 20px;
  }
`;
HeaderWrapper.displayName = 'HeaderWrapper';

export default HeaderWrapper;
