//*--------------------LIBRARIES--------------------*//
import React from 'react';
//*--------------INTERNAL DEPENDENCIES--------------*//
import HeaderWrapper from './index.style';
import IntlMessages from '../../helpers/languageProvider/intlMessages';

/**
 * AppHeader Component.
 *
 * This component render a simple title
 *
 */
const AppHeader = () => (
  <HeaderWrapper>
    <IntlMessages id='shipments.title' />
  </HeaderWrapper>
);

export default AppHeader;
