//*--------------------LIBRARIES--------------------*//
import React from 'react';
import { mount } from 'enzyme';
//*--------------INTERNAL DEPENDENCIES--------------*//
import AppHeader from './index';
import { Layout } from 'antd';

const { Header } = Layout;

describe('<AppHeader />', () => {
  let wrapper;

  beforeAll(() => {
    wrapper = mount(<AppHeader />);
  });

  it('should render AppHeader component', () => {
    expect(wrapper.find(Header)).toHaveLength(1);
  });

  it('AppHeader component renders correctly', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
