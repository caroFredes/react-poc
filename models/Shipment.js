const mongoose = require('mongoose');

const ShipmentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  cargo: [
    {
      type: {
        type: String
      },
      description: {
        type: String
      },
      volume: {
        type: String
      }
    }
  ],
  mode: {
    type: String
  },
  type: {
    type: String
  },
  destination: {
    type: String
  },
  origin: {
    type: String
  },
  userId: {
    type: String
  }
});

module.exports = Shipment = mongoose.model('shipment', ShipmentSchema);
